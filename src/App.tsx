import React from 'react';
// import logo from './logo.svg';
// import { Counter } from './features/counter/Counter';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';
import { Book, Home } from './pages';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/'>
          <Route  index element={<Home />} />
          <Route path='book/:id' element={<Book />} />
          <Route
            element={<Navigate to="/" />}
            path="*"
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
