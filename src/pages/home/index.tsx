import React from "react";
import { SearchInput } from "../../components";
import { Container, Row } from 'react-bootstrap';
import { SearchPanel } from "../../components/search-panel";
import { useAppSelector } from "../../app/hooks";
import { BookItem } from "../../components/book-item";

export const Home = () => {
  const { books } = useAppSelector(state => state.book);

  return (
    <Container className="mt-5">
      <Row className="position-relative">
        <SearchInput />
        <SearchPanel />
      </Row>
      <Row>
        {books.map(book => <BookItem key={book.id} {...book} />)}
      </Row>
    </Container>
  )
}