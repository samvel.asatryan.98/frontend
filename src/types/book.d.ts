interface IBook {
  author: string;
  title: string;
  description: string;
  rating: string;
  ratings_count: number;
  id: number;
  image: string;
}