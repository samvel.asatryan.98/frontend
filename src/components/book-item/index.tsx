import React from "react";
import { Card } from "react-bootstrap";


export const BookItem = ({ image, title, author, description, rating, ratings_count }: IBook) => {
  return (
    <Card className="mt-2 p-0">
      <Card.Header  as="h5">{title}</Card.Header>
      <Card.Img className="ms-3 mt-2" variant="top" src={image} style={{ width: 100 }} />
      <Card.Body>
        <Card.Title>{author}</Card.Title>
        <Card.Text dangerouslySetInnerHTML={{ __html: description }} />
      </Card.Body>
      <Card.Footer className="text-muted">{rating} rating from {ratings_count} votes</Card.Footer>
    </Card>
  )
}