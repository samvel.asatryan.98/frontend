import React, { useCallback, useEffect, useState } from "react";
import { InputGroup, Form } from "react-bootstrap";
import { useAppDispatch } from "../../app/hooks";
import axiosClient from '../../helpers/axios-client';
import { setBooks, setIsSearching, setMatchedBooks } from '../../redux/slices/book';

export const SearchInput = () => {
  const dispatch = useAppDispatch()
  const [search, setSearch] = useState('');
  const [data, setData] = useState([]);

  const onSearch = useCallback(async () => {
    const res = await axiosClient.get(`books?search=${search}`)
    setData(res.data);
    dispatch(setMatchedBooks(res.data));
    dispatch(setIsSearching(true))
  }, [dispatch, search]);
  
  useEffect(() => {
    if(search.trim()) {
      onSearch();
    } else {
      console.log('ssssss')
      dispatch(setIsSearching(false));
      dispatch(setMatchedBooks([]));
    }
  }, [onSearch, search, dispatch])
  
  const onSubmit = useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSearch('');
    dispatch(setBooks(data));
    dispatch(setIsSearching(false));
  }, [data, dispatch]);

  return (
    <Form onSubmit={onSubmit} className="p-0">
      <InputGroup>
        <Form.Control
          value={search}
          placeholder="searh book"
          aria-label="searh book"
          aria-describedby="basic-addon1"
          onChange={e => setSearch(e.target.value)}
          onFocus={() => {
            dispatch(setIsSearching(true))}}
          onBlur={() => {
            dispatch(setIsSearching(false))}}
        />
      </InputGroup>
    </Form>
  ) 
}