import React from "react";
import { useAppSelector } from "../../app/hooks";
import cn from 'classnames';
import { BookItem } from "../book-item";
import  './styles.css';

export const SearchPanel = () => {
  const { matchedBooks, isSearching } = useAppSelector(state => state.book);

  return (
    <div className={cn({ 'display-none': !matchedBooks.length || !isSearching }, 'book-panel')}>
      {matchedBooks.map(book => <BookItem key={book.id} {...book} />)}
    </div>
  )
}