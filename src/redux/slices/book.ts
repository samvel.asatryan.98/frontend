import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface BookState {
  books: IBook[];
  matchedBooks: IBook[];
  isSearching: boolean;
};

const initialState: BookState = {
  books: [],
  matchedBooks: [],
  isSearching: false,
};

export const bookSlice = createSlice({
  name: 'book',
  initialState,
  reducers: {
    setBooks: (state, action: PayloadAction<IBook[]>) => {
      state.books = action.payload;
    },
    setMatchedBooks: (state, action: PayloadAction<IBook[]>) => {
      state.matchedBooks = action.payload;
    },
    setIsSearching: (state, action: PayloadAction<boolean>) => {
      state.isSearching = action.payload;
    },
  },
});

export const { setBooks, setIsSearching, setMatchedBooks } = bookSlice.actions;

export default bookSlice.reducer;