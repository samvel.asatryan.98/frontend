import axios from "axios";

let config = {
  baseURL: process.env.REACT_APP_API_ENDPOINT,
  headers: {
    "Content-Type": "application/json",
  },
};

/** Creating the instance for axios */
const axiosClient = axios.create(config);

export default axiosClient;
